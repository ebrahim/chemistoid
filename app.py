from aiohttp import web
from aiohttp_wsgi import WSGIHandler
from application import application

wsgi_handler = WSGIHandler(application)
