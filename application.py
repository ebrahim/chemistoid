from aiohttp import web, web_request
from rdkit import Chem
from rdkit.Chem import Draw, AllChem

routes = web.RouteTableDef()

@routes.get('/v1/InChI={input:.+}')
async def inchi(request: web_request.Request):
    inchi = 'InChI=' + request.match_info.get('input')
    mol = Chem.MolFromInchi(inchi, removeHs=False)
    AllChem.EmbedMultipleConfs(mol)
    svg = Draw.MolToSVG(mol)
    return web.Response(text=svg, content_type='image/svg+xml')

@routes.get('/')
async def home():
    return web.Request(text='Chemistoid renderer using rdkit, https://gitlab.wikimedia.org/ebrahim/chemistoid')

app = web.Application()
app.add_routes(routes)
web.run_app(app)
